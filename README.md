# Quickstart dummy platform


## Quickstart
### What you’ll need
1. JDK 1.8 or later 
2. Maven 3.2+

### Run the application
How to start MySQL by docker, please execute:
```
 docker-compose up -d
```
How to start the application using Maven, execute:
```
 mvn package spring-boot:run
```
Then open index page
```
 http://localhost:8080
```

Swagger page
```
http://127.0.0.1:8080/swagger-ui.html
```
