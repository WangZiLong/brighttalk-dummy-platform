package com.utils.exceptions;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * @author Zilong Wang
 */
@XmlRootElement
public class Error implements Serializable {
    private static final long serialVersionUID = 1L;

    private String code;

    public Error() {
    }

    public Error(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
