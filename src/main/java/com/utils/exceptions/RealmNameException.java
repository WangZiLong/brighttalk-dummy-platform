package com.utils.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Zilong Wang
 */
@ResponseStatus(value= HttpStatus.BAD_REQUEST)
public class RealmNameException extends RuntimeException {

    public RealmNameException(String message) {
        super(message);
    }

}
