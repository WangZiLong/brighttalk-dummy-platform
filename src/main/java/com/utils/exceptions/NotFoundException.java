package com.utils.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Zilong Wang
 */
@ResponseStatus(value= HttpStatus.NOT_FOUND, reason="NotFound")
public class NotFoundException extends RuntimeException {
    public NotFoundException(String message) {
        super(message);
    }
}
