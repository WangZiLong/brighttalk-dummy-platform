package com.dao;

import com.model.AbstractObject;

import java.util.Optional;

/**
 * @author Zilong Wang
 */
public interface Dao<T extends AbstractObject> {
    /**
     * Create a instance in Database
     *
     * @param t
     * @return
     */
    T create(T t);

    /**
     * Select a row by id
     * @param id
     * @return
     */
    Optional<T> select(final int id);
}
