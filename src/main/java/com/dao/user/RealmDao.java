package com.dao.user;

import com.dao.Dao;
import com.dao.hibernate.AbstractDao;
import com.model.user.Realm;
import org.springframework.stereotype.Service;

/**
 * @author Zilong Wang
 */
public interface RealmDao extends Dao<Realm> {
    boolean existsByName(String name);
}
