package com.dao.hibernate.user;

import com.dao.hibernate.AbstractDao;
import com.dao.user.RealmDao;
import com.model.user.Realm;
import com.utils.exceptions.RealmNameException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * @author Zilong Wang
 */
@Service
public class RealmDaoImpl extends AbstractDao<Realm> implements RealmDao {
    public RealmDaoImpl() { super(Realm.class); }

    @Resource
    private RealmRepository realmRepository;

    @Override
    public boolean existsByName(String name) {
        Objects.requireNonNull(name);
        if (StringUtils.isEmpty(name))
            throw new RealmNameException("InvalidRealmName");
        return realmRepository.countByName(name) > 0;
    }

}
