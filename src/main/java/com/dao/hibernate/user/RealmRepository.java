package com.dao.hibernate.user;

import com.model.user.Realm;
import org.springframework.data.repository.CrudRepository;

/**
 * @author Zilong Wang
 */
public interface RealmRepository extends CrudRepository<Realm, Integer> {

    long countByName(String name);
}
