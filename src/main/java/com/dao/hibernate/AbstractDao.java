package com.dao.hibernate;


import com.dao.Dao;
import com.model.AbstractObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Optional;

/**
 * @author Zilong Wang
 */
public abstract class AbstractDao<T extends AbstractObject> implements Dao<T> {

    private final Class<T> clazz;

    public AbstractDao(Class<T> clazz) {
        this.clazz = clazz;
    }

    @Autowired
    protected CrudRepository<T, Integer> repository;

    @Autowired
    protected JdbcTemplate jdbcTemplate;

    public T create(T t) {
        return repository.save(t);
    }

    public Optional<T> select(int id) {
        return repository.findById(id);
    }
}
