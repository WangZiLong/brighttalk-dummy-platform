package com.controllers;

import com.utils.exceptions.ApiException;
import com.utils.exceptions.Error;
import com.utils.exceptions.RealmNameException;
import com.utils.exceptions.NotFoundException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(NotFoundException.class)
    protected ResponseEntity<Error> handleRealmNotFound(NotFoundException ex) {
        return buildResponseEntity(new Error("RealmNotFound"),NOT_FOUND);
    }

    @ExceptionHandler(RealmNameException.class)
    protected ResponseEntity<Error> handleRealmNameException(RealmNameException ex) {
        return buildResponseEntity(new Error(ex.getMessage()),BAD_REQUEST);
    }

    @ExceptionHandler(ApiException.class)
    protected ResponseEntity<Error> handleApiException(ApiException ex) {
        return buildResponseEntity(new Error(ex.getMessage()),BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    protected ResponseEntity<Error> handleException(Exception ex) {
        ex.printStackTrace();
        return buildResponseEntity(new Error(ex.getMessage()),BAD_REQUEST);
    }

    private ResponseEntity<Error> buildResponseEntity(Error error, HttpStatus status) {
        return new ResponseEntity<>(error, status);
    }
}