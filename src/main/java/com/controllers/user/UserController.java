package com.controllers.user;

import com.model.user.Realm;
import com.service.user.RealmService;
import com.service.user.RealmServiceImpl;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.logging.Logger;

/**
 * @author Zilong Wang
 */
@RestController
@RequestMapping(path = "/service/user")
@Api(value = "/service/user", tags = "user")
public class UserController{

    private static final Logger logger = Logger.getLogger(UserController.class.getName());

    @Autowired
    public UserController(RealmService realmService) {
        this.realmService = realmService;
    }

    private final RealmService realmService;

    @PostMapping(value = "/realm",
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
            consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<Realm> createRealm(@RequestBody Realm realm) {
        realmService.create(realm);
        return new ResponseEntity<Realm>(realm, HttpStatus.CREATED);
    }

    @GetMapping(path = "/realm/{id}",
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public Realm getRealm(@PathVariable("id") int id) {
        return realmService.select(id);
    }

}
