package com.service.user;

import com.dao.user.RealmDao;
import com.model.user.Realm;
import com.service.AbstractService;
import com.service.key.KeyService;
import com.utils.exceptions.RealmNameException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * @author Zilong Wang
 */
@Service
public class RealmServiceImpl extends AbstractService<Realm> implements RealmService {
    @Autowired
    public RealmServiceImpl(KeyService keyService, RealmDao realmDao) {
    super(Realm.class, realmDao);
        this.keyService = keyService;
        this.realmDao = realmDao;
    }

    private final RealmDao realmDao;

    private final KeyService keyService;

    @Override
    protected void checkBeforeCreate(Realm realm) {
        Objects.requireNonNull(realm);

        if (StringUtils.isEmpty(realm.getName()))   //Invalid Realm Name
            throw new RealmNameException("InvalidRealmName");

        if (realmDao.existsByName(realm.getName()))  //Duplicate Realm Name
            throw new RealmNameException("DuplicateRealmName");
    }

    @Override
    protected void doBeforeCreate(Realm realm) {
        realm.setId(null);
        realm.setKey(keyService.generate(realm));
    }
}
