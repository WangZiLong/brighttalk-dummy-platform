package com.service.user;

import com.model.user.Realm;
import com.service.Service;

/**
 * @author Zilong Wang
 */
public interface RealmService extends Service<Realm> {
}
