package com.service;

import com.model.AbstractObject;

/**
 * @author Zilong Wang
 */
public interface Service<T extends AbstractObject> {
    T create(T t);

    T select(final int id);
}
