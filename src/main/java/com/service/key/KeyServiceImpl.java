package com.service.key;

import com.model.user.Realm;
import org.springframework.stereotype.Service;

import java.util.Base64;
import java.util.Objects;
import java.util.Random;

@Service
public class KeyServiceImpl implements KeyService {

    public static final int KEY_LENGTH = 32;

    private static final Random rand = new Random();


    /**
     * Generate 32 chars encryption key
     *
     * Algorithm:
     *    base64(name + timestamp).sub[0,31]
     *
     * @param realm: Realm
     */
    @Override
    public String generate(Realm realm) {
        Objects.requireNonNull(realm);
        Objects.requireNonNull(realm.getName(),"Realm's name cannot be null.");//todo custom exception.
        String seed = realm.getName() + System.currentTimeMillis() + base64(KEY_LENGTH);
        return Base64.getEncoder().encodeToString(seed.getBytes()).substring(0, KEY_LENGTH);
    }

    private static byte[] bytes(int length) {
        byte[] b = new byte[length];
        rand.nextBytes(b);
        return b;
    }

    private static String base64(int length) {
        return Base64.getEncoder().encodeToString(bytes(length)).substring(0, length);
    }
}
