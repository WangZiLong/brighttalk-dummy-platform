package com.service.key;

import com.model.user.Realm;

public interface KeyService {
    /**
     * Generate encryption key.
     *
     * @param realm: Realm
     * @return 32 char encryption key.
     */
    String generate(Realm realm);
}
