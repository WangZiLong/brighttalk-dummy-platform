package com.service;


import com.dao.Dao;
import com.model.AbstractObject;
import com.utils.exceptions.ApiException;
import com.utils.exceptions.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Zilong Wang
 */
public abstract class AbstractService<T  extends AbstractObject> implements Service<T> {

    protected Dao<T> dao;

    private final Class<T> clazz;

    @Autowired
    public AbstractService(Class<T> clazz, Dao<T> dao) {
        this.clazz = clazz;
        this.dao = dao;
    }


    @Override
    public T create(T t) {
        checkBeforeCreate(t);
        doBeforeCreate(t);
        return dao.create(t);
    }

    protected void doBeforeCreate(T t) {
    }

    protected void checkBeforeCreate(T t){

    }

    @Override
    public T select(int id) {
        if (id <= 0)
            throw new ApiException("InvalidArgument");

        return dao.select(id)
                .orElseThrow(() -> new NotFoundException(clazz.getSimpleName() + "NotFound"));
    }
}
