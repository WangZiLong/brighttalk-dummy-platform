package com.model;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAttribute;
import java.io.Serializable;

/**
 * @author Zilong Wang
 */
@MappedSuperclass
public abstract class AbstractObject implements Serializable, Cloneable{
    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE)
    @Column(updatable = false, nullable = false)
    protected Integer id;

    @XmlAttribute
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
