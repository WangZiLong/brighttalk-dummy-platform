package com.model.user;

import com.model.AbstractObject;
import io.swagger.annotations.ApiModel;
import org.springframework.stereotype.Repository;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Zilong Wang
 */
@ApiModel(value = "realm", description = "realm model")
@XmlRootElement(name = "realm")
@Repository
@Entity
@Table(name = "realm")
public class Realm extends AbstractObject {
    @Column(unique = true)
    private String name;

    @Column(columnDefinition="VARCHAR(255)")
    private String description;

    @Column(name = "_key", columnDefinition="CHAR(32)", nullable = false)
    private String key;

    @XmlAttribute
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
