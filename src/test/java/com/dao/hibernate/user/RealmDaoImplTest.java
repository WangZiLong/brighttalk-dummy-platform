package com.dao.hibernate.user;

import com.utils.exceptions.RealmNameException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RealmDaoImplTest {

    @Mock
    private RealmRepository realmRepository;

    @InjectMocks
    private RealmDaoImpl realmDaoImpl;


    /**
     * Test realm's name is exist or not.
     *
     * test cases:
     * 1. name null
     * 2. name empty
     * 3. name exist
     * 4. name not exist
     *
     */
    @Test(expected = NullPointerException.class)
    public void nameNull() {
        realmDaoImpl.existsByName(null);
    }

    @Test(expected = RealmNameException.class)
    public void nameEmpty() {
        realmDaoImpl.existsByName("");
    }

    @Test
    public void nameNotExist() {
        when(realmRepository.countByName(anyString())).thenReturn(0l);
        boolean expect = realmDaoImpl.existsByName("Zilong");
        Assert.assertEquals(expect, false);

    }

    @Test
    public void nameExist() {
        when(realmRepository.countByName(anyString())).thenReturn(1l);
        boolean expect = realmDaoImpl.existsByName("Zilong");
        Assert.assertEquals(expect, true);

    }

}