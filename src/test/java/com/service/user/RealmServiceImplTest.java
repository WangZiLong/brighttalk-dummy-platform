package com.service.user;

import com.dao.user.RealmDao;
import com.model.user.Realm;
import com.service.key.KeyService;
import com.utils.exceptions.ApiException;
import com.utils.exceptions.NotFoundException;
import com.utils.exceptions.RealmNameException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RealmServiceImplTest {

    @Mock
    private RealmDao realmDao;

    @Mock
    private KeyService keyService;


    @InjectMocks
    private RealmServiceImpl realmServiceImpl;

    /**
     * checkBeforeCreate Test case:
     * 1. null
     * 2. name is null | empty | invalid
     *
     */
    @Test(expected = NullPointerException.class)
    public void checkBeforeCreateNull() {
        realmServiceImpl.checkBeforeCreate(null);
    }

    @Test(expected = RealmNameException.class)
    public void checkBeforeCreateNameNull() {
        Realm realm = new Realm();
        realmServiceImpl.checkBeforeCreate(realm);
    }

    @Test(expected = RealmNameException.class)
    public void checkBeforeCreateNameEmpty() {
        Realm realm = new Realm();
        realm.setName("");
        realmServiceImpl.checkBeforeCreate(realm);
    }

    @Test(expected = RealmNameException.class)
    public void checkBeforeCreateNameDuplicate() {
        //given
        Realm realm = new Realm();
        realm.setName("Zilong");
        when(realmDao.existsByName(realm.getName())).thenReturn(true);

        //when
        realmServiceImpl.checkBeforeCreate(realm);
    }


    @Test
    public void checkBeforeCreateNameValid() {
        //given
        Realm realm = new Realm();
        realm.setName("Zilong");
        when(realmDao.existsByName(realm.getName())).thenReturn(false);

        //when
        realmServiceImpl.checkBeforeCreate(realm);
    }

    @Test
    public void doBeforeCreate() {
        //given
        Realm realm = new Realm();
        realm.setName("Zilong");
        realm.setDescription("desc");

        when(keyService.generate(realm)).thenReturn("11111111111111111111111111111111");

        //when
        realmServiceImpl.doBeforeCreate(realm);

        //then
        Assert.assertNotNull(realm.getKey());
        Assert.assertEquals(32, realm.getKey().length());
    }


    /**
     * Get Realm test case:
     * 1. id <= 0
     * 2. id not found
     * 3. id found
     *
     */
    @Test(expected = ApiException.class)
    public void getRealmIdInvalid() {
        realmServiceImpl.select(-1);
    }

    @Test(expected = NotFoundException.class)
    public void getRealmIdNotFound() {
        when(realmDao.select(anyInt())).thenReturn(Optional.empty());

        realmServiceImpl.select(9999999);
    }

    @Test
    public void getRealm() {
        //given
        Realm realm = new Realm();
        realm.setId(1);
        realm.setName("Zilong");
        realm.setDescription("desc");

        when(realmDao.select(realm.getId())).thenReturn(Optional.of(realm));

        Realm r = realmServiceImpl.select(realm.getId());

        Assert.assertNotNull(r);
        Assert.assertEquals(realm.getId(), r.getId());

    }

}