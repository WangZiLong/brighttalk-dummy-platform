package com.service.key;

import com.model.user.Realm;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class KeyServiceImplTest {

    @InjectMocks
    private KeyServiceImpl keyServiceImpl;

    @Test
    public void generate() {
        //given
        Realm realm = new Realm();
        realm.setName("test name");
        realm.setDescription("Desc");

        //when
        String key = keyServiceImpl.generate(realm);

        //then
        Assert.assertEquals(KeyServiceImpl.KEY_LENGTH, key.length());
    }

    @Test(expected = NullPointerException.class)
    public void generateNameNull() {
        //given
        Realm realm = new Realm();

        //when
        keyServiceImpl.generate(realm);

    }
}