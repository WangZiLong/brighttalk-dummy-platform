package com.controllers.user;

import com.model.user.Realm;
import com.service.user.RealmServiceImpl;
import com.utils.exceptions.ApiException;
import com.utils.exceptions.NotFoundException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static sun.plugin2.util.PojoUtil.toJson;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
public class UserControllerTest {

    @MockBean
    private RealmServiceImpl realmServiceImpl;

    @MockBean
    private UserController userController;

    @Autowired
    private MockMvc mockMvc;

    /**
     * Create realm API ( no enough time)
     * 1. with id
     * 2. without name
     * 3. name is empty
     * 4. without description
     * 5. description empty
     * 6. with key
     * 7. name is not supplied:
     * 8. realm name matches the name of an existing realm.
     * 9. return xml
     * 10. return json
     *
     * Get realm
     * 1. id <= 0
     * 2. id is not an integer value
     * 3. id not found
     * 4. return json
     * 5. return XML
     *
     * */
    @Test
    public void getRealmJsonIdInvalid() throws Exception {
        when(userController.getRealm(anyInt())).thenThrow(new ApiException("InvalidArgument"));

        this.mockMvc.perform( MockMvcRequestBuilders
            .get("/service/user/realm/{id}",-1)
            .header("Origin","*")
            .accept(MediaType.APPLICATION_JSON))
            .andDo(print())
            .andExpect(status().isBadRequest())
            .andExpect(MockMvcResultMatchers.jsonPath("$.code").exists())
            .andExpect(MockMvcResultMatchers.jsonPath("$.code").value("InvalidArgument"));
    }

    @Test
    public void getRealmXmlIdInvalid() throws Exception {
        when(userController.getRealm(anyInt())).thenThrow(new ApiException("InvalidArgument"));

        this.mockMvc.perform( MockMvcRequestBuilders
                .get("/service/user/realm/{id}",-1)
                .header("Origin","*")
                .accept(MediaType.APPLICATION_XML))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.xpath("/error/code").exists())
                .andExpect(MockMvcResultMatchers.xpath("/error/code").string("InvalidArgument"));
    }

    @Test
    public void getRealmXmlIdNotFound() throws Exception {
        when(userController.getRealm(anyInt())).thenThrow(new NotFoundException("RealmNotFound"));

        this.mockMvc.perform( MockMvcRequestBuilders
                .get("/service/user/realm/{id}",1)
                .header("Origin","*")
                .accept(MediaType.APPLICATION_XML))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.xpath("/error/code").exists())
                .andExpect(MockMvcResultMatchers.xpath("/error/code").string("RealmNotFound"));
    }

    @Test
    public void getRealmJsonIdNotFound() throws Exception {
        when(userController.getRealm(anyInt())).thenThrow(new NotFoundException("RealmNotFound"));

        this.mockMvc.perform(MockMvcRequestBuilders
                .get("/service/user/realm/{id}", -1)
                .header("Origin", "*")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$.code").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.code").value("RealmNotFound"));
    }

    @Test
    public void getRealmXmlIdFound() throws Exception {

        Realm realm = new Realm();
        realm.setId(1);
        realm.setName("test_name");
        realm.setDescription("desc");
        realm.setKey("key");

        when(userController.getRealm(realm.getId())).thenReturn(realm);

        this.mockMvc.perform( MockMvcRequestBuilders
                .get("/service/user/realm/{id}",realm.getId())
                .header("Origin","*")
                .accept(MediaType.APPLICATION_XML))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.xpath("/realm/@name").string(realm.getName()))
                .andExpect(MockMvcResultMatchers.xpath("/realm/@id").string(realm.getId().toString()))
                .andExpect(MockMvcResultMatchers.xpath("/realm/key").string(realm.getKey()))
                .andExpect(MockMvcResultMatchers.xpath("/realm/description").string(realm.getDescription()));
    }

    @Test
    public void getRealmJsonIdFound() throws Exception {
        Realm realm = new Realm();
        realm.setId(1);
        realm.setName("test_name");
        realm.setDescription("desc");
        realm.setKey("key");

        when(userController.getRealm(realm.getId())).thenReturn(realm);

        this.mockMvc.perform(MockMvcRequestBuilders
                .get("/service/user/realm/{id}", realm.getId())
                .header("Origin", "*")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(realm.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(realm.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.description").value(realm.getDescription()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.key").value(realm.getKey()));
    }

    @Test
    public void create() throws Exception {
        Realm realm = new Realm();
        realm.setId(1);
        realm.setName("test_name");
        realm.setDescription("desc");
        realm.setKey("key");

        when(userController.createRealm(any())).thenReturn(new ResponseEntity<Realm>(realm, HttpStatus.CREATED));

        this.mockMvc.perform(MockMvcRequestBuilders
                .post("/service/user/realm")
                .header("Origin", "*")
                .content(toJson(realm))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(realm.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(realm.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.description").value(realm.getDescription()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.key").value(realm.getKey()));
    }
}
